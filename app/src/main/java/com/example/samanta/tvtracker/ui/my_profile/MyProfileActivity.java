package com.example.samanta.tvtracker.ui.my_profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.ui.login_register.LoginActivity;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyProfileActivity extends AppCompatActivity {

    @BindView(R.id.edittext_editEmail)
    EditText editEmail;
    @BindView(R.id.textview_numberOfWatchedEpisodes_number)
    TextView numberOfWatchedEpisodes;
    @BindView(R.id.textview_timeSpentOnWatching_number)
    TextView timeSpentOnWatching;
    @BindView(R.id.toolbarMyProfile)
    Toolbar toolbar;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private DatabaseReference databaseReference, databaseReference2;
    private Query query;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

       // mAuth = FirebaseAuth.getInstance();
        //user = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchedEpisodes");
        databaseReference.keepSynced(true);
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("watchedMovies");
        databaseReference2.keepSynced(true);

        getEmail();
        getNumberOfWatchedEpisodes();
        getNumberOfWatchedMovies();
    }


    private void getEmail() {
        if (user != null) {
            user = mAuth.getCurrentUser();
            String userEmail = user.getEmail().toString();
            editEmail.setText(userEmail);
        } else if (user == null){
            Toast.makeText(this,"User null",Toast.LENGTH_SHORT).show();
           editEmail.setText("No available email");
        }

    }

    @OnClick(R.id.button_deleteAccount)
    public void deleteAccount() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete account?")
                .setTitle("Delete Account")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteConfirmed();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(MyProfileActivity.this, "Account not deleted",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void deleteConfirmed() {
        user.delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("TAG", "User account deleted.");
                            startActivity(new Intent(MyProfileActivity.this, LoginActivity.class));
                        } else {
                            Toast.makeText(MyProfileActivity.this,
                                    "Deleting account failed. Please try again",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @OnClick(R.id.button_editEmail)
    public void changeEmail() {
        final String newEmail = editEmail.getText().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to change email?")
                .setTitle("Change email")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        changeConfirmed(newEmail);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(MyProfileActivity.this, "Email not changed",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();

    }

    private void changeConfirmed(String email) {
        user.updateEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText
                                    (MyProfileActivity.this,
                                            "Email successfully changed",
                                            Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void getNumberOfWatchedEpisodes(){

        query = databaseReference.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        counter++;
                    }
                    numberOfWatchedEpisodes.setText(String.valueOf(counter));
                    counter = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getNumberOfWatchedMovies(){
        query = databaseReference2.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        counter++;
                    }
                    timeSpentOnWatching.setText(String.valueOf(counter));
                    counter = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;

            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
               //startActivity(new Intent(this, WatchlistActivity.class));
                finish();
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                String query = item.getTitle().toString();

                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...

        return true;
    }


}
