package com.example.samanta.tvtracker.ui.tv_show.details.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.TvShowSeasonClickListener;
import com.example.samanta.tvtracker.pojo.TvShowSeasons;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TvShowDetailsSeasonsAdapter extends RecyclerView.Adapter<TvShowDetailsSeasonsAdapter.TvShowSeasonsViewHolder> {

    private final List<TvShowSeasons> tvShowSeasons;
    private TvShowSeasonClickListener tvShowSeasonClickListener;

    public TvShowDetailsSeasonsAdapter() {
        tvShowSeasons = new ArrayList<>();
    }

    public TvShowDetailsSeasonsAdapter(TvShowSeasonClickListener listener) {
        this.tvShowSeasonClickListener = listener;
        this.tvShowSeasons = new ArrayList<>();
    }

    public void setTvShowSeasons(List<TvShowSeasons> tvShowSeasons) {
        if(tvShowSeasons!=null) {
            this.tvShowSeasons.clear();
            this.tvShowSeasons.addAll(tvShowSeasons);
            notifyDataSetChanged();
        }
    }


    @NonNull
    @Override
    public TvShowSeasonsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seasons_item, parent, false);
        return new TvShowSeasonsViewHolder(view, tvShowSeasonClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowSeasonsViewHolder holder, int position) {

        TvShowSeasons seasons = tvShowSeasons.get(position);

        holder.seasonNumber.setText(seasons.getSeasonName());
        holder.seasonDescription.setText(seasons.getSeasonDescription());
        holder.episodeNumber.setText("Episode number: " + seasons.getEpisodeNumber());

    }

    @Override
    public int getItemCount() {
        return tvShowSeasons.size();
    }


    class TvShowSeasonsViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textview_seasonNumber)
        TextView seasonNumber;
        @BindView(R.id.textview_seasonDescription)
        TextView seasonDescription;
        @BindView(R.id.textview_seasonEpisodeNumber)
        TextView episodeNumber;

        public TvShowSeasonsViewHolder(View itemView, TvShowSeasonClickListener listener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick
        public void onSeasonClick(){
            tvShowSeasonClickListener.onClick(tvShowSeasons.get(getAdapterPosition()));
        }
    }
}
