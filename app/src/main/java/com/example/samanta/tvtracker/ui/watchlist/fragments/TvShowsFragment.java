package com.example.samanta.tvtracker.ui.watchlist.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.WatchlistClickListener;
import com.example.samanta.tvtracker.pojo.WatchlistMovies;
import com.example.samanta.tvtracker.pojo.WatchlistTvShows;
import com.example.samanta.tvtracker.ui.tv_show.details.TvShowDetailsActivity;
import com.example.samanta.tvtracker.ui.watchlist.adapters.WatchlistTvShowsAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TvShowsFragment extends Fragment {

    private DatabaseReference databaseReference;
    private List<WatchlistTvShows> tvShowsList;
    private FirebaseDatabase database;
    private FirebaseUser user;
    private FirebaseAuth auth;
    WatchlistTvShows tvShows;
    Query query, query2;

    WatchlistTvShowsAdapter adapter;
    @BindView(R.id.recycler_watchlistTvShows)
    RecyclerView watchlistTvShows;

    WatchlistClickListener watchlistClickListener = new WatchlistClickListener() {
        @Override
        public void onClick(WatchlistMovies watchlistMovies) {

        }

        @Override
        public void onClick(WatchlistTvShows watchlist) {
            tvShowDetails(watchlist);
        }

        @Override
        public void onLongClick(WatchlistMovies watchlistMovies) {

        }

        @Override
        public void onLongClick(WatchlistTvShows watchlistTvShows) {
            removeFromWatchlist(watchlistTvShows);
        }
    };

    public TvShowsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_shows, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        ButterKnife.bind(this, view);

        tvShowsList = new ArrayList<>();


        adapter = new WatchlistTvShowsAdapter(watchlistClickListener, tvShowsList);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        watchlistTvShows.setLayoutManager(linearLayoutManager);
        watchlistTvShows.setItemAnimator(new DefaultItemAnimator());
        watchlistTvShows.setAdapter(adapter);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference().child("watchlistTvShows");
        databaseReference.keepSynced(true);
        query = databaseReference.orderByChild("userID").equalTo(user.getUid());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    private void tvShowDetails(WatchlistTvShows watchlist) {
        Activity activity = this.getActivity();
        Intent intent = new Intent(activity, TvShowDetailsActivity.class);
        Bundle extras = new Bundle();

        extras.putString("TV_SHOW_TITLE", watchlist.getTvShowTitle());
        extras.putInt("TV_SHOW_ID", Integer.valueOf(watchlist.getTvShowID()));
        extras.putString("TV_SHOW_POSTER", watchlist.getTvShowPoster());
        extras.putString("TV_SHOW_DESCRIPTION", watchlist.getTvShowDescription());
        extras.putString("TV_SHOW_RELEASE_DATE", watchlist.getTvShowFirstAirDate());
        extras.putString("TV_SHOW_NUMBER_OF_SEASONS", watchlist.getNumberOfSeasons());
        extras.putString("TV_SHOW_NUMBER_OF_EPISODES", watchlist.getNumberOfEpisodes());

        intent.putExtras(extras);
        startActivity(intent);
    }

    private void fetchData(DataSnapshot dataSnapshot) {

        if (dataSnapshot != null) {
            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                tvShows = new WatchlistTvShows();

                tvShows.setUserID(ds.getValue(WatchlistTvShows.class).getUserID());
                tvShows.setTvShowID(ds.getValue(WatchlistTvShows.class).getTvShowID());
                tvShows.setNumberOfEpisodes(ds.getValue(WatchlistTvShows.class).getNumberOfEpisodes());
                tvShows.setNumberOfSeasons(ds.getValue(WatchlistTvShows.class).getNumberOfSeasons());
                tvShows.setTvShowDescription(ds.getValue(WatchlistTvShows.class).getTvShowDescription());
                tvShows.setTvShowFirstAirDate(ds.getValue(WatchlistTvShows.class).getTvShowFirstAirDate());
                tvShows.setTvShowPoster(ds.getValue(WatchlistTvShows.class).getTvShowPoster());
                tvShows.setTvShowTitle(ds.getValue(WatchlistTvShows.class).getTvShowTitle());
                tvShows.setUserEmail(ds.getValue(WatchlistTvShows.class).getUserEmail());

                tvShowsList.add(tvShows);
            }
            adapter = new WatchlistTvShowsAdapter(watchlistClickListener, tvShowsList);
            watchlistTvShows.setAdapter(adapter);
        }
    }

    private void removeFromWatchlist(final WatchlistTvShows watchlistTvShow){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    query2 = databaseReference.orderByChild("tvShowID").equalTo(watchlistTvShow.getTvShowID());
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {

                                builder.setMessage("Do you want to delete "
                                        + watchlistTvShow.getTvShowTitle() +
                                        " from watchlist?")
                                        .setTitle("Delete from watchlist")
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                                                    ds.getRef().removeValue();
                                                    Toast.makeText(getActivity(),
                                                            watchlistTvShow.getTvShowTitle() +
                                                                    " sucessfully deleted.",
                                                            Toast.LENGTH_SHORT).show();
                                                    updateView();

                                                }
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(getActivity(), watchlistTvShow.getTvShowTitle()
                                                                + " was not deleted from watchlist.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void updateView(){

        tvShowsList.clear();
        adapter = new WatchlistTvShowsAdapter(watchlistClickListener, tvShowsList);
        watchlistTvShows.setAdapter(adapter);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}
