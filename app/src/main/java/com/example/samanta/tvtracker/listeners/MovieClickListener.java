package com.example.samanta.tvtracker.listeners;

import com.example.samanta.tvtracker.pojo.Movie;

public interface MovieClickListener {

    void onClick(Movie movie);
    void onLongClick(Movie movie);

}
