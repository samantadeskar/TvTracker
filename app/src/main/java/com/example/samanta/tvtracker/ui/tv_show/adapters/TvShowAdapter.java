package com.example.samanta.tvtracker.ui.tv_show.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.TvShowClickListener;
import com.example.samanta.tvtracker.pojo.TvShow;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.TvShowViewHolder> {

    private final List<TvShow> tvShows;
    private TvShowClickListener tvShowClickListener;

    public TvShowAdapter() {
        tvShows = new ArrayList<>();
    }

    public TvShowAdapter(TvShowClickListener listener) {
        tvShowClickListener = listener;
        tvShows = new ArrayList<>();
    }

    public void setTvShows(List<TvShow> tvShows) {
        if (tvShows != null) {
            this.tvShows.clear();
            this.tvShows.addAll(tvShows);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public TvShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tvshow_view, parent, false);
        return new TvShowViewHolder(view, tvShowClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowViewHolder holder, int position) {

        TvShow tvShow = tvShows.get(position);

        if (tvShow.getPoster() != null) {
            Picasso.with(holder.itemView.getContext())
                    .load(tvShow.getPoster())
                    .into(holder.tvShowPoster);
        }
        holder.tvShowTitle.setText(tvShow.getTitle());
        holder.tvShowDescription.setText(tvShow.getDescription());
    }

    @Override
    public int getItemCount() {
        return tvShows.size();
    }

    public void addTvShows(List<TvShow> tvShows) {
        this.tvShows.addAll(tvShows);
        notifyDataSetChanged();
    }

    class TvShowViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_tvShowImage)
        ImageView tvShowPoster;
        @BindView(R.id.textview_tvShowTitle)
        TextView tvShowTitle;
        @BindView(R.id.textview_tvShowDescription)
        TextView tvShowDescription;


        public TvShowViewHolder(View itemView, TvShowClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick
        public void onTvShowClick() {
            tvShowClickListener.onClick(tvShows.get(getAdapterPosition()));
        }

        @OnLongClick
        public boolean onTvShowLongClick() {
            tvShowClickListener.onLongClick(tvShows.get(getAdapterPosition()));
            return true;
        }

    }
}
