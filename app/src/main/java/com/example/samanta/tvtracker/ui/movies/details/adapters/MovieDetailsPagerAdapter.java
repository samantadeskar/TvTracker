package com.example.samanta.tvtracker.ui.movies.details.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.samanta.tvtracker.ui.movies.details.fragments.MovieDetailsCommentFragment;
import com.example.samanta.tvtracker.ui.movies.details.fragments.MovieDetailsDescriptionFragment;

import java.util.ArrayList;
import java.util.List;

public class MovieDetailsPagerAdapter extends FragmentStatePagerAdapter{

    int numOfTabs;
    private final List<Fragment> fragmentList = new ArrayList<>();


    public MovieDetailsPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    public void setItems(List<Fragment> fragments){
        fragmentList.clear();
        fragmentList.addAll(fragments);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MovieDetailsDescriptionFragment descriptionTab = new MovieDetailsDescriptionFragment();
                return descriptionTab;
            case 1:
                MovieDetailsCommentFragment commentTab = new MovieDetailsCommentFragment();
                return commentTab;
            default:
                MovieDetailsDescriptionFragment descriptionTab2 = new MovieDetailsDescriptionFragment();
                return descriptionTab2;
        }
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
