package com.example.samanta.tvtracker.pojo;

import com.example.samanta.tvtracker.constants.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WatchlistMovies {

    private String movieID;
    private String userID;
    private String userEmail;
    private String movieTitle;
    private String moviePoster;
    private String movieDescription;
    private String movieReleaseDate;

    public WatchlistMovies(){}

    public WatchlistMovies(String movieDescription,String movieID,String moviePoster,
                           String movieReleaseDate,String movieTitle,
                           String userEmail,String userID){
        this.movieID = movieID;
        this.userID = userID;
        this.userEmail = userEmail;
        this.movieTitle = movieTitle;
        this.moviePoster = moviePoster;
        this.movieDescription = movieDescription;
        this.movieReleaseDate = movieReleaseDate;
    }

    public String getMovieID() {
        return movieID;
    }

    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMoviePoster() {
        return moviePoster;
    }

    public void setMoviePoster(String moviePoster) {
        this.moviePoster = moviePoster;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getMovieReleaseDate() {
        return movieReleaseDate;
    }

    public void setMovieReleaseDate(String movieReleaseDate) {
        this.movieReleaseDate = movieReleaseDate;
    }
}
