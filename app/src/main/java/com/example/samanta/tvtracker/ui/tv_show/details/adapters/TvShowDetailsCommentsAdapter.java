package com.example.samanta.tvtracker.ui.tv_show.details.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.pojo.TvShowComments;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TvShowDetailsCommentsAdapter extends RecyclerView.Adapter<TvShowDetailsCommentsAdapter.TvShowCommentsViewHolder> {

    private final List<TvShowComments> tvShowComments;

    public TvShowDetailsCommentsAdapter(){
        tvShowComments = new ArrayList<>();
    }

    public TvShowDetailsCommentsAdapter(List<TvShowComments> tvShowComments){
        this.tvShowComments = tvShowComments;
    }

    public void setTvShowComments(List<TvShowComments> comments){
        this.tvShowComments.clear();
        this.tvShowComments.addAll(comments);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TvShowCommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item,parent,false);
        return new TvShowCommentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowCommentsViewHolder holder, int position) {
        TvShowComments comments = tvShowComments.get(position);
        holder.userNameComment.setText(comments.getUserName());
        holder.userComment.setText(comments.getComment());
    }

    @Override
    public int getItemCount() {
        return tvShowComments.size();
    }

    public void addComments(List<TvShowComments> comments){
        this.tvShowComments.addAll(comments);
        notifyDataSetChanged();
    }

    class TvShowCommentsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_userNameComment)
        TextView userNameComment;
        @BindView(R.id.textview_userComment)
        TextView userComment;


        public TvShowCommentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
