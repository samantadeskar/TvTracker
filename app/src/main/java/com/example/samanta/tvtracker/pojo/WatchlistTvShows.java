package com.example.samanta.tvtracker.pojo;

import com.example.samanta.tvtracker.constants.Constants;

public class WatchlistTvShows {
    private String tvShowID;
    private String userID;
    private String userEmail;
    private String tvShowTitle;
    private String tvShowPoster;
    private String tvShowDescription;
    private String tvShowFirstAirDate;
    private String numberOfSeasons;
    private String numberOfEpisodes;

    public WatchlistTvShows() {
    }

    public WatchlistTvShows(String tvShowID, String userID, String userEmail,
                            String tvShowTitle, String tvShowPoster,
                            String tvShowDescription, String tvShowFirstAirDate,
                            String numberOfSeasons, String numberOfEpisodes) {
        this.tvShowID = tvShowID;
        this.userID = userID;
        this.userEmail = userEmail;
        this.tvShowTitle = tvShowTitle;
        this.tvShowPoster = tvShowPoster;
        this.tvShowDescription = tvShowDescription;
        this.tvShowFirstAirDate = tvShowFirstAirDate;
        this.numberOfSeasons = numberOfSeasons;
        this.numberOfEpisodes = numberOfEpisodes;
    }

    public String getTvShowID() {
        return tvShowID;
    }

    public void setTvShowID(String tvShowID) {
        this.tvShowID = tvShowID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getTvShowTitle() {
        return tvShowTitle;
    }

    public void setTvShowTitle(String tvShowTitle) {
        this.tvShowTitle = tvShowTitle;
    }

    public String getTvShowPoster() {
        return tvShowPoster;
    }

    public void setTvShowPoster(String tvShowPoster) {
        this.tvShowPoster = tvShowPoster;
    }

    public String getTvShowDescription() {
        return tvShowDescription;
    }

    public void setTvShowDescription(String tvShowDescription) {
        this.tvShowDescription = tvShowDescription;
    }

    public String getTvShowFirstAirDate() {
        return tvShowFirstAirDate;
    }

    public void setTvShowFirstAirDate(String tvShowFirstAirDate) {
        this.tvShowFirstAirDate = tvShowFirstAirDate;
    }

    public String getNumberOfSeasons() {
        return numberOfSeasons;
    }

    public void setNumberOfSeasons(String numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }

    public String getNumberOfEpisodes() {
        return numberOfEpisodes;
    }

    public void setNumberOfEpisodes(String numberOfEpisodes) {
        this.numberOfEpisodes = numberOfEpisodes;
    }
}
