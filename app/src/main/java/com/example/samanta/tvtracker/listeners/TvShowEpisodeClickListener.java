package com.example.samanta.tvtracker.listeners;

import com.example.samanta.tvtracker.pojo.TvShowEpisode;

public interface TvShowEpisodeClickListener {

    void onClick(TvShowEpisode tvShowEpisode);
}
