package com.example.samanta.tvtracker.ui.movies.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.interaction.MoviesInteractor;
import com.example.samanta.tvtracker.listeners.EndlessScrollListener;
import com.example.samanta.tvtracker.listeners.MovieClickListener;
import com.example.samanta.tvtracker.networking.RetrofitUtil;
import com.example.samanta.tvtracker.pojo.Movie;
import com.example.samanta.tvtracker.response.MovieResponse;
import com.example.samanta.tvtracker.ui.movies.adapters.MoviesAdapter;
import com.example.samanta.tvtracker.ui.movies.details.MovieDetailsActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopularMoviesFragment extends Fragment {

    private final MoviesInteractor interactor = RetrofitUtil.getMoviesInteractor();
    MoviesAdapter adapter;
    @BindView(R.id.recycler_popularMovies)
    RecyclerView popularMovies;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private FirebaseAuth auth;

    Query query;
    Query query2;

    MovieClickListener movieClickListener = new MovieClickListener() {

        @Override
        public void onClick(Movie movie) {
            movieDetails(movie);
        }

        @Override
        public void onLongClick(final Movie movie) {

            query = databaseReference.orderByChild("userID").equalTo(user.getUid());
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        query2 = databaseReference.orderByChild("movieID").equalTo(String.valueOf(movie.getId()));
                        query2.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    Toast.makeText(getActivity(),
                                            movie.getTitle() + " is already on watchlist",
                                            Toast.LENGTH_SHORT).show();
                                } else {

                                    addToWatchlistAlert(movie);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    };

    public PopularMoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        interactor.getPopularMovies(1, getMoviesCallback());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_popular_movies, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        adapter = new MoviesAdapter(movieClickListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        popularMovies.setLayoutManager(linearLayoutManager);
        popularMovies.setItemAnimator(new DefaultItemAnimator());
        popularMovies.setAdapter(adapter);

        EndlessScrollListener scrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                interactor.getPopularMovies(page, addMoviesCallback());
            }
        };
        popularMovies.addOnScrollListener(scrollListener);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchlistMovies");
        databaseReference.keepSynced(true);

    }


    private Callback<MovieResponse> getMoviesCallback() {
        return new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    adapter.setMovies(response.body().getMovies());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.d("TAG", "Fail");
            }
        };
    }

    private Callback<MovieResponse> addMoviesCallback() {
        return new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    adapter.addMovies(response.body().getMovies());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.d("TAG", "Fail");
            }
        };
    }

    private void movieDetails(Movie movie) {

        Activity activity = this.getActivity();
        Intent intent = new Intent(activity, MovieDetailsActivity.class);
        Bundle extras = new Bundle();

        extras.putString("MOVIE_TITLE", movie.getTitle());
        extras.putInt("MOVIE_ID", movie.getId());
        extras.putString("MOVIE_POSTER", movie.getPoster());
        extras.putString("MOVIE_DESCRIPTION", movie.getDescription());
        extras.putString("MOVIE_RELEASE_DATE", movie.getReleaseDate());

        intent.putExtras(extras);
        startActivity(intent);

    }

    private void addToWatchlistAlert(final Movie movie) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Do you want to add " + movie.getTitle() + " to watchlist?")
                .setTitle("Add to watchlist")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addToWatchlist(movie);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getActivity(), movie.getTitle() + " was not added to watchlist.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }


    private void addToWatchlist(final Movie movie) {

        DatabaseReference newEntry = databaseReference.push();

        String movieID = String.valueOf(movie.getId());
        String userID = user.getUid();
        String userEmail = user.getEmail();
        String movieTitle = movie.getTitle();
        String moviePoster = movie.getPoster();
        String movieDescription = movie.getDescription();
        String movieReleaseDate = movie.getReleaseDate();

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("movieID", movieID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);
        dataToSave.put("movieTitle", movieTitle);
        dataToSave.put("moviePoster", moviePoster);
        dataToSave.put("movieDescription", movieDescription);
        dataToSave.put("movieReleaseDate", movieReleaseDate);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), movie.getTitle() + " succesfully added to watchlist",
                        Toast.LENGTH_SHORT).show();
            }
        });


    }


}
