package com.example.samanta.tvtracker.ui.search;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.interaction.MoviesInteractor;
import com.example.samanta.tvtracker.listeners.EndlessScrollListener;
import com.example.samanta.tvtracker.listeners.MovieClickListener;
import com.example.samanta.tvtracker.networking.RetrofitUtil;
import com.example.samanta.tvtracker.pojo.Movie;
import com.example.samanta.tvtracker.response.MovieResponse;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.example.samanta.tvtracker.ui.movies.adapters.MoviesAdapter;
import com.example.samanta.tvtracker.ui.movies.details.MovieDetailsActivity;
import com.example.samanta.tvtracker.ui.my_profile.MyProfileActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.example.samanta.tvtracker.ui.watchlist.WatchlistActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchMoviesActivity extends AppCompatActivity {

    private final MoviesInteractor interactor = RetrofitUtil.getMoviesInteractor();
    private MoviesAdapter adapter;
    @BindView(R.id.recycler_search)
    RecyclerView searchMovies;
    @BindView(R.id.toolbar_search)
    Toolbar toolbar_searchMovie;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private FirebaseAuth auth;

    MovieClickListener movieClickListener = new MovieClickListener(){

        @Override
        public void onClick(Movie movie) {
            movieDetails(movie);
        }

        @Override
        public void onLongClick(Movie movie) {
            addToWatchlistAlert(movie);
        }
    };

    private void toastMovie(Movie movie) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        final String searchQuery = getIntent().getExtras().getString("SEARCH_QUERY");

        adapter = new MoviesAdapter(movieClickListener);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        searchMovies.setLayoutManager(linearLayoutManager);
        searchMovies.setItemAnimator(new DefaultItemAnimator());
        searchMovies.setAdapter(adapter);
        EndlessScrollListener scrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int TotalItemCount, RecyclerView view) {
                interactor.getSearchedMovie(page, addMoviesCallback(), searchQuery);
            }
        };
        searchMovies.addOnScrollListener(scrollListener);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchlist");
        databaseReference.keepSynced(true);

        setSupportActionBar(toolbar_searchMovie);

    }


    @Override
    public void onStart() {
        super.onStart();
        String searchQuery = getIntent().getExtras().getString("SEARCH_QUERY");
        interactor.getSearchedMovie(1, getMoviesCallback(), searchQuery);
    }

    private Callback<MovieResponse> getMoviesCallback() {
        return new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    adapter.setMovies(response.body().getMovies());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.d("TAG", "Fail");

            }
        };
    }

    private Callback<MovieResponse> addMoviesCallback() {
        return new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    adapter.addMovies(response.body().getMovies());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.d("TAG", "Fail");
            }
        };
    }

    private void movieDetails(Movie movie){
        Intent intent = new Intent(this, MovieDetailsActivity.class);
        Bundle extras = new Bundle();

        extras.putString("MOVIE_TITLE", movie.getTitle());
        extras.putInt("MOVIE_ID", movie.getId());
        extras.putString("MOVIE_POSTER", movie.getPoster());
        extras.putString("MOVIE_DESCRIPTION", movie.getDescription());
        extras.putString("MOVIE_RELEASE_DATE", movie.getReleaseDate());

        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }

    private void addToWatchlistAlert(final Movie movie) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to add " + movie.getTitle() + " to watchlist?")
                .setTitle("Add to watchlist")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addToWatchlist(movie);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(SearchMoviesActivity.this, movie.getTitle() + " was not added to watchlist.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void addToWatchlist(final Movie movie) {

        DatabaseReference newEntry = databaseReference.push();

        String movieID = String.valueOf(movie.getId());
        String userID = user.getUid();
        String userEmail = user.getEmail();

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("movieID", movieID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(SearchMoviesActivity.this, movie.getTitle() + " succesfully added to watchlist",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;
            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
                startActivity(new Intent(this, WatchlistActivity.class));
                finish();
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager =
                (SearchManager)getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getInput(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                String query = item.getTitle().toString();

                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...
        return true;
    }

    private void getInput(String query) {
        Intent explicitIntent = new Intent(this, SearchMoviesActivity.class);
        explicitIntent.putExtra("SEARCH_QUERY", query);
        startActivity(explicitIntent);
    }

}
