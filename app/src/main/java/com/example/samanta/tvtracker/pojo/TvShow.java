package com.example.samanta.tvtracker.pojo;

import com.example.samanta.tvtracker.constants.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShow {

    @SerializedName("original_name")
    private String title;

    @SerializedName("id")
    private int id;

    @SerializedName("poster_path")
    private String poster;

    @SerializedName("overview")
    private String description;

    @SerializedName("backdrop_path")
    private String backdrop;

    @SerializedName("first_air_date")
    private String firstAirDate;


    @SerializedName("number_of_seasons")
    private int numberOfSeasons;

    public int getNumberOfSeasons() {
        return numberOfSeasons;
    }

    public void setNumberOfSeasons(int numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }

    public int getNumberOfEpisodes() {
        return numberOfEpisodes;
    }

    public void setNumberOfEpisodes(int numberOfEpisodes) {
        this.numberOfEpisodes = numberOfEpisodes;
    }

    @SerializedName("number_of_episodes")
    private int numberOfEpisodes;


    public TvShow() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return Constants.TMDB_IMAGE_FILE_PATH + poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public void setFirstAirDate(String firstAirDate) {
        this.firstAirDate = firstAirDate;
    }

    public static class TvShowResult {
        private List<TvShow> tvShowResults;

        public List<TvShow> getTvShowResults() {
            return tvShowResults;
        }
    }
}
