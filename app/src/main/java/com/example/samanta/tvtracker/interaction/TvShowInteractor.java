package com.example.samanta.tvtracker.interaction;

import com.example.samanta.tvtracker.response.TvShowEpisodeResponse;
import com.example.samanta.tvtracker.response.TvShowResponse;
import com.example.samanta.tvtracker.response.TvShowSeasonsResponse;

import retrofit2.Callback;


public interface TvShowInteractor {

    void getPopularTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback);

    void getTopRatedTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback);

    void getLatestTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback);

    void getOnTheAirTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback);

    void getSearchedTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback, String query);

    void getTvShowSeasons(int tv_id, Callback<TvShowSeasonsResponse> tvShowSeasonsResponseCallback);

    void getTvShowEpisodes(int tv_id, int season_number, Callback<TvShowEpisodeResponse> tvShowEpisodeResponseCallback);
}
