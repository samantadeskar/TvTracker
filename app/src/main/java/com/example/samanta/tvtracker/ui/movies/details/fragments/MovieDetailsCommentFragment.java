package com.example.samanta.tvtracker.ui.movies.details.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.pojo.MovieComments;
import com.example.samanta.tvtracker.ui.movies.details.adapters.MovieDetailsCommentsAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MovieDetailsCommentFragment extends Fragment {

    @BindView(R.id.recycler_movieComments)
    RecyclerView recycler_movieComments;
    @BindView(R.id.edittext_enterCommentMovie)
    EditText enterCommentMovie;

    MovieDetailsCommentsAdapter adapter;

    private List<MovieComments> movieCommentsList;
    private MovieComments movieComments;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private FirebaseAuth auth;
    private Query query;


    public MovieDetailsCommentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_details_comment, container,
                false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        movieCommentsList = new ArrayList<>();

        adapter = new MovieDetailsCommentsAdapter(movieCommentsList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler_movieComments.setLayoutManager(linearLayoutManager);
        recycler_movieComments.setItemAnimator(new DefaultItemAnimator());
        recycler_movieComments.setAdapter(adapter);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("movieComments");
        databaseReference.keepSynced(true);

        query = databaseReference.orderByChild("movieID")
                .equalTo(String.valueOf(getActivity()
                        .getIntent().getExtras().getInt("MOVIE_ID")));
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchData(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null) {
            for (DataSnapshot ds : dataSnapshot.getChildren()) {

                movieComments = new MovieComments();

                movieComments.setUserName(ds.getValue(MovieComments.class).getUserName());
                movieComments.setComment(ds.getValue(MovieComments.class).getComment());
                movieComments.setMovieID(ds.getValue(MovieComments.class).getMovieID());
                movieCommentsList.add(movieComments);
            }
            adapter = new MovieDetailsCommentsAdapter(movieCommentsList);
            recycler_movieComments.setAdapter(adapter);
        }
    }

    @OnClick(R.id.button_sendCommentMovie)
    public void commentMovie() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Do you want to send comment?")
                .setTitle("Comment movie")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendComment();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(),
                                "Comment was not sent.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void sendComment() {

        String userName = user.getUid();
        String comment = enterCommentMovie.getText().toString();
        String movieID = String.valueOf(getActivity().getIntent().getExtras().getInt("MOVIE_ID"));

        DatabaseReference newEntry = databaseReference.push();

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("userName", userName);
        dataToSave.put("comment", comment);
        dataToSave.put("movieID", movieID);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(),
                        "Comment succesfully sent.",
                        Toast.LENGTH_SHORT).show();
            }
        });
        updateView();
    }

    public void updateView(){

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
