package com.example.samanta.tvtracker.ui.movies.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.MovieClickListener;
import com.example.samanta.tvtracker.pojo.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {

    private MovieClickListener movieClickListener;
    private final List<Movie> movies;


    public MoviesAdapter(){
        movies = new ArrayList<>();
    }

    public MoviesAdapter(MovieClickListener listener){
        movieClickListener = listener;
        movies = new ArrayList<>();
    }


    public void setMovies(List<Movie> movies) {
        if (movies != null) {
            this.movies.clear();
            this.movies.addAll(movies);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_view, parent, false);
        return new MovieViewHolder(view, movieClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        Movie movie = movies.get(position);
        if (movie.getPoster() != null) {
            Picasso.with(holder.itemView.getContext())
                    .load(movie.getPoster())
                    .into(holder.moviePoster);
        }
        holder.movieTitle.setText(movie.getTitle());
        holder.movieDescription.setText(movie.getDescription());
        //holder.setMovies(movie);


    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void addMovies(List<Movie> movies) {
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_movieImage)
        ImageView moviePoster;
        @BindView(R.id.textview_movieTitle)
        TextView movieTitle;
        @BindView(R.id.textview_movieDescription)
        TextView movieDescription;


        public MovieViewHolder(View itemView, MovieClickListener listener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick
        public void onMovieClick(){

            movieClickListener.onClick(movies.get(getAdapterPosition()));
        }

        @OnLongClick
        public boolean onMovieLongClick(){

            movieClickListener.onLongClick(movies.get(getAdapterPosition()));
            return true;
        }

    }
}
