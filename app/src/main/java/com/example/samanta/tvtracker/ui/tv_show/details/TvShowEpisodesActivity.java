package com.example.samanta.tvtracker.ui.tv_show.details;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.interaction.TvShowInteractor;
import com.example.samanta.tvtracker.listeners.TvShowEpisodeClickListener;
import com.example.samanta.tvtracker.networking.RetrofitUtil;
import com.example.samanta.tvtracker.pojo.TvShowEpisode;
import com.example.samanta.tvtracker.response.TvShowEpisodeResponse;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.example.samanta.tvtracker.ui.my_profile.MyProfileActivity;
import com.example.samanta.tvtracker.ui.search.SearchTvShowActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.example.samanta.tvtracker.ui.tv_show.details.adapters.TvShowDetailsEpisodeAdapter;
import com.example.samanta.tvtracker.ui.watchlist.WatchlistActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowEpisodesActivity extends AppCompatActivity {

    private final TvShowInteractor interactor = RetrofitUtil.getTvShowInteractor();
    private TvShowDetailsEpisodeAdapter adapter = new TvShowDetailsEpisodeAdapter();

    @BindView(R.id.recycler_episodes)
    RecyclerView recycler_episodes;
    @BindView(R.id.textview_seasonNumberTitle)
    TextView seasonNumberTitle;
    @BindView(R.id.toolbarEpisodes)
    Toolbar toolbarEpisodes;

    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference databaseReference;
    Query query, query2;

    private int tv_id, season_number;

    TvShowEpisodeClickListener tvShowEpisodeClickListener = new TvShowEpisodeClickListener() {
        @Override
        public void onClick(TvShowEpisode tvShowEpisode) {
            checkIfWatched(tvShowEpisode);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodes);
        ButterKnife.bind(this);

        adapter = new TvShowDetailsEpisodeAdapter(tvShowEpisodeClickListener);

        tv_id = getIntent().getExtras().getInt("TV_SHOW_ID");
        season_number = getIntent().getExtras().getInt("SEASON_NUMBER");

        seasonNumberTitle.setText("Season " + season_number);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_episodes.setLayoutManager(linearLayoutManager);
        recycler_episodes.setItemAnimator(new DefaultItemAnimator());
        recycler_episodes.setAdapter(adapter);
        interactor.getTvShowEpisodes(tv_id, season_number, getTvShowEpisodeCallback());

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchedEpisodes");
        databaseReference.keepSynced(true);

        setSupportActionBar(toolbarEpisodes);

    }

    private Callback<TvShowEpisodeResponse> getTvShowEpisodeCallback() {

        return new Callback<TvShowEpisodeResponse>() {
            @Override
            public void onResponse(Call<TvShowEpisodeResponse> call, Response<TvShowEpisodeResponse> response) {
                if (response.isSuccessful()) {
                    adapter.setTvShowEpisodes(response.body().getShowEpisodes());
                }
            }

            @Override
            public void onFailure(Call<TvShowEpisodeResponse> call, Throwable t) {
                Log.d("TAG", "Fail");
            }
        };
    }

    private void checkIfWatched(final TvShowEpisode tvShowEpisode){

        query = databaseReference.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    query2 = databaseReference.orderByChild("episodeID").equalTo(String.valueOf(tvShowEpisode.getEpisode_id()));
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                Toast.makeText(TvShowEpisodesActivity.this,
                                        "Episode already watched.",
                                        Toast.LENGTH_SHORT).show();
                            }
                            else {
                                markAsWatchedAlert(tvShowEpisode);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }else {
                    markAsWatchedAlert(tvShowEpisode);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void markAsWatchedAlert(final TvShowEpisode tvShowEpisode) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Mark as watched?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference newEntry = databaseReference.push();

                        String tvShowID = String.valueOf(getIntent().getExtras().getInt("TV_SHOW_ID"));
                        String seasonNumber = String.valueOf(getIntent().getExtras().getInt("SEASON_NUMBER"));
                        String episodeAirDate = tvShowEpisode.getEpisodeAirDate();
                        String episodeNumber = String.valueOf(tvShowEpisode.getEpisode_number());
                        String episodeDescription = tvShowEpisode.getEpisodeDescription();
                        String episodeID = String.valueOf(tvShowEpisode.getEpisode_id());
                        String episodeName = tvShowEpisode.getEpisodeName();
                        String userEmail = user.getEmail();
                        String userID = user.getUid();

                        Map<String, String> dataToSave = new HashMap<>();
                        dataToSave.put("tvShowID", tvShowID);
                        dataToSave.put("seasonNumber", seasonNumber);
                        dataToSave.put("episodeAirDate", episodeAirDate);
                        dataToSave.put("episodeNumber", episodeNumber);
                        dataToSave.put("episodeDescription", episodeDescription);
                        dataToSave.put("episodeID", episodeID);
                        dataToSave.put("episodeName", episodeName);
                        dataToSave.put("userEmail", userEmail);
                        dataToSave.put("userID", userID);

                        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(TvShowEpisodesActivity.this, "Marked as watched.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(TvShowEpisodesActivity.this, "Not watched.", Toast.LENGTH_SHORT).show();
                    }
                }).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;

            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
                startActivity(new Intent(this, WatchlistActivity.class));
                finish();
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager =
                (SearchManager)getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getInput(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...

        return true;
    }

    private void getInput(String query) {
        Intent explicitIntent = new Intent(this, SearchTvShowActivity.class);
        explicitIntent.putExtra("SEARCH_TVSHOW_QUERY", query);
        startActivity(explicitIntent);
    }

}
