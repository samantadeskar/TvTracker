package com.example.samanta.tvtracker.ui.tv_show.details.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TvShowDetailsDescriptionFragment extends Fragment {

    @BindView(R.id.textview_firstAirDate)
    TextView firstAirDate;
    @BindView(R.id.textView_tvShowDetailsSummary)
    TextView summary;


    public TvShowDetailsDescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tv_show_details_description, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        ButterKnife.bind(this, view);
        setDetails();
        summary.setMovementMethod(new ScrollingMovementMethod());

    }

    public void setDetails() {

        String airDate = getActivity().getIntent().getExtras().getString("TV_SHOW_RELEASE_DATE");
        String description = getActivity().getIntent().getExtras().getString("TV_SHOW_DESCRIPTION");
        String voteAverage = String.valueOf(getActivity().getIntent().getExtras().getInt("TV_SHOW_VOTE_AVERAGE"));

        firstAirDate.setText(airDate);
        summary.setText(description);
    }
}
