package com.example.samanta.tvtracker.interaction;

import com.example.samanta.tvtracker.response.MovieResponse;

import retrofit2.Callback;


public interface MoviesInteractor {

    void getPopularMovies(int page, Callback<MovieResponse> movieResponseCallback);

    void getNowPlayingMovies(int page, Callback<MovieResponse> movieResponseCallback);

    void getTopRatedMovies(int page, Callback<MovieResponse> movieResponseCallback);

    void getCommingSoonMovies(int page, Callback<MovieResponse> movieResponseCallback);

    void getSearchedMovie(int page, Callback<MovieResponse> movieResponseCallback, String query);

}









