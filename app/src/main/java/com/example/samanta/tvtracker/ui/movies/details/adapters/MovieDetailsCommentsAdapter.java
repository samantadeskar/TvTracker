package com.example.samanta.tvtracker.ui.movies.details.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.pojo.MovieComments;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsCommentsAdapter extends RecyclerView.Adapter<MovieDetailsCommentsAdapter.MovieCommentsViewHolder> {

    private final List<MovieComments> movieComments;

    public MovieDetailsCommentsAdapter(){
        movieComments = new ArrayList<>();
    }

    public MovieDetailsCommentsAdapter(List<MovieComments> movieComments){
        this.movieComments = movieComments;
    }

    public void setMovieComments(List<MovieComments> comments){

        if(comments!=null){
            this.movieComments.clear();
            this.movieComments.addAll(comments);
            notifyDataSetChanged();
        }

    }

    @NonNull
    @Override
    public MovieCommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item,parent,false);
        return new MovieCommentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieCommentsViewHolder holder, int position) {
        MovieComments comments = movieComments.get(position);
        holder.userNameComment.setText(comments.getUserName());
        holder.userComment.setText(comments.getComment());
    }

    @Override
    public int getItemCount() {
        return movieComments.size();
    }

    public void addComments(List<MovieComments> comments){
        this.movieComments.addAll(comments);
        notifyDataSetChanged();
    }

    class MovieCommentsViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textview_userNameComment)
        TextView userNameComment;

        @BindView(R.id.textview_userComment)
        TextView userComment;

        public MovieCommentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
