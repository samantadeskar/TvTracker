package com.example.samanta.tvtracker.ui.watchlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.example.samanta.tvtracker.ui.my_profile.MyProfileActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.example.samanta.tvtracker.ui.watchlist.adapters.WatchlistMoviesAdapter;
import com.example.samanta.tvtracker.ui.watchlist.adapters.WatchlistPagerAdapter;
import com.example.samanta.tvtracker.ui.watchlist.fragments.MoviesFragment;
import com.example.samanta.tvtracker.ui.watchlist.fragments.TvShowsFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WatchlistActivity extends AppCompatActivity {

    @BindView(R.id.toolbarWatchlist)
    Toolbar toolbar;
    @BindView(R.id.fragmentContanier_Watchlist)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watchlist);
        ButterKnife.bind(this);

        WatchlistPagerAdapter adapter = new WatchlistPagerAdapter(getSupportFragmentManager());
        List<Fragment> pages = new ArrayList<>();
        pages.add(new MoviesFragment());
        pages.add(new TvShowsFragment());
        adapter.setItems(pages);
        viewPager.setAdapter(adapter);

        setSupportActionBar(toolbar);



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;

            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
                startActivity(new Intent(this, WatchlistActivity.class));
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();


        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                String query = item.getTitle().toString();

                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...

        return true;
    }
}
