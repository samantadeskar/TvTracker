package com.example.samanta.tvtracker.response;

import com.example.samanta.tvtracker.pojo.TvShowSeasons;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShowSeasonsResponse {

    @SerializedName("seasons")
    private List<TvShowSeasons> showSeasons;

    public List<TvShowSeasons> getShowSeasons() {
        return showSeasons;
    }

    public void setShowSeasons(List<TvShowSeasons> showSeasons) {
        this.showSeasons = showSeasons;
    }
}
