package com.example.samanta.tvtracker.ui.watchlist.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.WatchlistClickListener;
import com.example.samanta.tvtracker.pojo.WatchlistTvShows;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class WatchlistTvShowsAdapter extends RecyclerView.Adapter<WatchlistTvShowsAdapter.WatchlistTvShowsViewHolder> {

    private WatchlistClickListener watchlistClickListener;
    private final List<WatchlistTvShows> watchlists;

    public WatchlistTvShowsAdapter() {
        watchlists = new ArrayList<>();
    }

    public WatchlistTvShowsAdapter(WatchlistClickListener listener, List<WatchlistTvShows> watchlists) {
        this.watchlistClickListener = listener;
        this.watchlists = watchlists;
    }

    public void setWatchlists(List<WatchlistTvShows> watchlists) {
        if (watchlists != null) {
            this.watchlists.clear();
            this.watchlists.addAll(watchlists);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public WatchlistTvShowsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.watchlist_item, parent, false);
        return new WatchlistTvShowsViewHolder(view, watchlistClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchlistTvShowsViewHolder holder, int position) {

        WatchlistTvShows watchlist = watchlists.get(position);
        if (watchlist.getTvShowPoster() != null) {
            Picasso.with(holder.itemView.getContext())
                    .load(watchlist.getTvShowPoster())
                    .into(holder.movieTvShowImage);
        }
        holder.movieTvShowTitle.setText(watchlist.getTvShowTitle());
        holder.movieTvShowDescription.setText(watchlist.getTvShowDescription());

    }

    @Override
    public int getItemCount() {
        return watchlists.size();
    }

    public void addWatchlistTvShows(List<WatchlistTvShows> watchlists) {
        this.watchlists.addAll(watchlists);
        notifyDataSetChanged();
    }


    public class WatchlistTvShowsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_movieTvShowImage)
        ImageView movieTvShowImage;
        @BindView(R.id.textview_movieTvShowTitle)
        TextView movieTvShowTitle;
        @BindView(R.id.textview_movieTvShowDescription)
        TextView movieTvShowDescription;


        public WatchlistTvShowsViewHolder(View itemView, WatchlistClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        public void onItemClick() {
            watchlistClickListener.onClick(watchlists.get(getAdapterPosition()));
        }

        @OnLongClick
        public boolean onItemLongClick(){
            watchlistClickListener.onLongClick(watchlists.get(getAdapterPosition()));
            return true;
        }


    }
}
