package com.example.samanta.tvtracker.ui.movies.details.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MovieDetailsDescriptionFragment extends Fragment {

    @BindView(R.id.textView_movieDetailsSummary)
    TextView summary;
    @BindView(R.id.textview_releaseDate)
    TextView releaseDate;

    private Intent intent;
    private Bundle extras;



    public MovieDetailsDescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_details_description, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        ButterKnife.bind(this, view);

        setDetails();
        summary.setMovementMethod(new ScrollingMovementMethod());


    }

    public void setDetails() {


        String description = getActivity().getIntent().getExtras().getString("MOVIE_DESCRIPTION");
        summary.setText(description);
        summary.setMovementMethod(new ScrollingMovementMethod());


        String movie_release_date = getActivity().getIntent().getExtras().getString("MOVIE_RELEASE_DATE");
        releaseDate.setText(movie_release_date);

    }

}
