package com.example.samanta.tvtracker.ui.tv_show.details.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.pojo.TvShowComments;
import com.example.samanta.tvtracker.ui.tv_show.details.adapters.TvShowDetailsCommentsAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TvShowDetailsCommentFragment extends Fragment {

    @BindView(R.id.recycler_tvShowComments)
    RecyclerView recycler_tvShowComments;
    @BindView(R.id.edittext_enterCommentTvShow)
    EditText enterCommentTvShow;

    TvShowDetailsCommentsAdapter adapter;

    private List<TvShowComments> tvShowCommentsList;
    private TvShowComments tvShowComments;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private FirebaseAuth auth;
    private Query query;

    public TvShowDetailsCommentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_show_details_comment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        tvShowCommentsList = new ArrayList<>();

        adapter = new TvShowDetailsCommentsAdapter(tvShowCommentsList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler_tvShowComments.setLayoutManager(linearLayoutManager);
        recycler_tvShowComments.setItemAnimator(new DefaultItemAnimator());
        recycler_tvShowComments.setAdapter(adapter);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("tvShowComments");
        databaseReference.keepSynced(true);

        query = databaseReference.orderByChild("tvShowID")
                .equalTo(String.valueOf(getActivity().getIntent()
                        .getExtras().getInt("TV_SHOW_ID")));
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void fetchData(DataSnapshot dataSnapshot) {

        if (dataSnapshot != null) {
            for (DataSnapshot ds : dataSnapshot.getChildren()) {

                tvShowComments = new TvShowComments();

                tvShowComments.setUserName(ds.getValue(TvShowComments.class).getUserName());
                tvShowComments.setComment(ds.getValue(TvShowComments.class).getComment());
                tvShowComments.setTvShowID(ds.getValue(TvShowComments.class).getTvShowID());
                tvShowCommentsList.add(tvShowComments);
            }
            adapter = new TvShowDetailsCommentsAdapter(tvShowCommentsList);
            recycler_tvShowComments.setAdapter(adapter);
        }
    }

    @OnClick(R.id.button_sendCommentTvShow)
    public void commentTvShow() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Do you want to send comment?")
                .setTitle("Comment tv show")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendComment();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Comment was not sent."
                                , Toast.LENGTH_SHORT).show();
                    }
                }).show();
    }

    private void sendComment() {

        String userName = user.getUid();
        String comment = enterCommentTvShow.getText().toString();
        String tvShowID = String.valueOf(getActivity().getIntent().getExtras().getInt("TV_SHOW_ID"));

        DatabaseReference newEntry = databaseReference.push();

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("userName", userName);
        dataToSave.put("comment", comment);
        dataToSave.put("tvShowID", tvShowID);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(),
                        "Comment succesfully sent.",
                        Toast.LENGTH_SHORT).show();
            }
        });
        updateView();
    }

    public void updateView(){

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
