package com.example.samanta.tvtracker.listeners;

import com.example.samanta.tvtracker.pojo.TvShowSeasons;

public interface TvShowSeasonClickListener {

    void onClick(TvShowSeasons tvShowSeasons);

}
