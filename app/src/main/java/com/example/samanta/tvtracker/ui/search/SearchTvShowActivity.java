package com.example.samanta.tvtracker.ui.search;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.interaction.TvShowInteractor;
import com.example.samanta.tvtracker.listeners.EndlessScrollListener;
import com.example.samanta.tvtracker.listeners.TvShowClickListener;
import com.example.samanta.tvtracker.networking.RetrofitUtil;
import com.example.samanta.tvtracker.pojo.TvShow;
import com.example.samanta.tvtracker.response.TvShowResponse;
import com.example.samanta.tvtracker.ui.tv_show.adapters.TvShowAdapter;
import com.example.samanta.tvtracker.ui.tv_show.details.TvShowDetailsActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTvShowActivity extends AppCompatActivity {

    private final TvShowInteractor interactor = RetrofitUtil.getTvShowInteractor();
    private TvShowAdapter adapter = new TvShowAdapter();
    @BindView(R.id.recycler_searchTvShow)
    RecyclerView searchTvShow;
    @BindView(R.id.toolbar_searchTvShow)
    Toolbar toolbar_searchTvShow;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private FirebaseAuth auth;

    TvShowClickListener tvShowClickListener = new TvShowClickListener() {
        @Override
        public void onClick(TvShow tvShow) {
            tvShowDetails(tvShow);
        }

        @Override
        public void onLongClick(TvShow tvShow) {
            addToWatchlistAlert(tvShow);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_tv_show);
        ButterKnife.bind(this);

        final String searchQuery = getIntent().getExtras().getString("SEARCH_TVSHOW_QUERY");

        adapter = new TvShowAdapter(tvShowClickListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        searchTvShow.setLayoutManager(linearLayoutManager);
        searchTvShow.setItemAnimator(new DefaultItemAnimator());
        searchTvShow.setAdapter(adapter);
        EndlessScrollListener scrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int TotalItemCount, RecyclerView view) {
                interactor.getSearchedTvShow(page, addTvShowCallback(), searchQuery);
            }
        };
        searchTvShow.addOnScrollListener(scrollListener);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchlist");
        databaseReference.keepSynced(true);

        setSupportActionBar(toolbar_searchTvShow);
    }

    @Override
    public void onStart() {
        super.onStart();
        String searchQuery = getIntent().getExtras().getString("SEARCH_TVSHOW_QUERY");
        interactor.getSearchedTvShow(1, getTvShowCallback(), searchQuery);
    }

    private Callback<TvShowResponse> addTvShowCallback() {
        return new Callback<TvShowResponse>() {
            @Override
            public void onResponse(Call<TvShowResponse> call, Response<TvShowResponse> response) {
                if(response.isSuccessful()){
                    adapter.addTvShows(response.body().getTvShows());
                }
            }

            @Override
            public void onFailure(Call<TvShowResponse> call, Throwable t) {
                Log.d("TAG", "Fail");
            }
        };
    }



    private Callback<TvShowResponse> getTvShowCallback() {
        return new Callback<TvShowResponse>() {
            @Override
            public void onResponse(Call<TvShowResponse> call, Response<TvShowResponse> response) {
                if(response.isSuccessful()){
                    adapter.setTvShows(response.body().getTvShows());
                }
            }

            @Override
            public void onFailure(Call<TvShowResponse> call, Throwable t) {
                Log.d("TAG", "FAIL");
            }
        };
    }

    private void tvShowDetails(TvShow tvShow){
        Intent intent = new Intent(this, TvShowDetailsActivity.class);
        Bundle extras = new Bundle();

        extras.putString("TV_SHOW_TITLE", tvShow.getTitle());
        extras.putInt("TV_SHOW_ID", tvShow.getId());
        extras.putString("TV_SHOW_POSTER", tvShow.getPoster());
        extras.putString("TV_SHOW_DESCRIPTION", tvShow.getDescription());
        extras.putString("TV_SHOW_RELEASE_DATE", tvShow.getFirstAirDate());
        extras.putString("TV_SHOW_NUMBER_OF_SEASONS", (String.valueOf(tvShow.getNumberOfSeasons())));
        extras.putString("TV_SHOW_NUMBER_OF_EPISODES", (String.valueOf(tvShow.getNumberOfEpisodes())));

        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }

    private void addToWatchlistAlert(final TvShow tvShow) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to add " + tvShow.getTitle() + " to watchlist?")
                .setTitle("Add to watchlist")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addToWatchlist(tvShow);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(SearchTvShowActivity.this, tvShow.getTitle() + " was not added to watchlist.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void addToWatchlist(final TvShow tvShow) {

        DatabaseReference newEntry = databaseReference.push();

        String tvShowID = String.valueOf(tvShow.getId());
        String userID = user.getUid();
        String userEmail = user.getEmail();
        String tvShowTitle = tvShow.getTitle();
        String tvShowPoster = tvShow.getPoster();
        String tvShowDescription = tvShow.getDescription();
        String tvShowFirstAirDate = tvShow.getFirstAirDate();
        String numberOfSeasons = String.valueOf(tvShow.getNumberOfSeasons());
        String numberOfEpisodes = String.valueOf(tvShow.getNumberOfEpisodes());

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("tvShowID", tvShowID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);
        dataToSave.put("tvShowTitle", tvShowTitle);
        dataToSave.put("tvShowPoster", tvShowPoster);
        dataToSave.put("tvShowDescription", tvShowDescription);
        dataToSave.put("tvShowFirstAirDate", tvShowFirstAirDate);
        dataToSave.put("numberOfSeasons", numberOfSeasons);
        dataToSave.put("numberOfEpisodes", numberOfEpisodes);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(SearchTvShowActivity.this, tvShow.getTitle() + " succesfully added to watchlist",
                        Toast.LENGTH_SHORT).show();
            }
        });


    }
}
