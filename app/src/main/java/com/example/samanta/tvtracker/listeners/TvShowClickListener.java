package com.example.samanta.tvtracker.listeners;

import com.example.samanta.tvtracker.pojo.TvShow;

public interface TvShowClickListener {

    void onClick(TvShow tvShow);
    void onLongClick(TvShow tvShow);
}
