package com.example.samanta.tvtracker.listeners;

import com.example.samanta.tvtracker.pojo.WatchlistMovies;
import com.example.samanta.tvtracker.pojo.WatchlistTvShows;

public interface WatchlistClickListener {

    void onClick(WatchlistMovies watchlistMovies);
    void onClick (WatchlistTvShows watchlistTvShows);

    void onLongClick(WatchlistMovies watchlistMovies);
    void onLongClick(WatchlistTvShows watchlistTvShows);
}
