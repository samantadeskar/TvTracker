package com.example.samanta.tvtracker.ui.watchlist.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class WatchlistPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fragmentList = new ArrayList<>();

    public WatchlistPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    public void setItems(List<Fragment> fragments){
        fragmentList.clear();
        fragmentList.addAll(fragments);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
