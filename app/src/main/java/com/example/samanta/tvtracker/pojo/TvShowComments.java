package com.example.samanta.tvtracker.pojo;

public class TvShowComments {

    String userName;
    String comment;
    String tvShowID;

    public TvShowComments() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTvShowID() {
        return tvShowID;
    }

    public void setTvShowID(String tvShowID) {
        this.tvShowID = tvShowID;
    }
}
