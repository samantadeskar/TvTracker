package com.example.samanta.tvtracker.ui.tv_show.details.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.TvShowEpisodeClickListener;
import com.example.samanta.tvtracker.pojo.TvShowEpisode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TvShowDetailsEpisodeAdapter extends RecyclerView.Adapter<TvShowDetailsEpisodeAdapter.TvShowDetailsEpisodeViewHolder> {

    private final List<TvShowEpisode> tvShowEpisodes;
    private TvShowEpisodeClickListener episodeClickListener;

    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference databaseReference;
    Query query, query2;

    public TvShowDetailsEpisodeAdapter(){
        tvShowEpisodes = new ArrayList<>();
    }

    public TvShowDetailsEpisodeAdapter(TvShowEpisodeClickListener listener){
        this.episodeClickListener = listener;
        this.tvShowEpisodes = new ArrayList<>();
    }

    public void setTvShowEpisodes(List<TvShowEpisode> episodes){
        if(episodes!=null){
            this.tvShowEpisodes.clear();
            this.tvShowEpisodes.addAll(episodes);
            notifyDataSetChanged();
        }
    }


    @NonNull
    @Override
    public TvShowDetailsEpisodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.episode_item,parent,false);
        return new TvShowDetailsEpisodeViewHolder(view, episodeClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowDetailsEpisodeViewHolder holder, int position) {

        TvShowEpisode episode = tvShowEpisodes.get(position);

        holder.episodeNumber.setText("Episode " +episode.getEpisode_number()+": ");
        holder.episodeAirDate.setText(episode.getEpisodeAirDate());
        holder.episodeDescription.setText(episode.getEpisodeDescription());
        holder.episodeName.setText(episode.getEpisodeName());
    }

    @Override
    public int getItemCount() {
        return tvShowEpisodes.size();
    }

    class TvShowDetailsEpisodeViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textview_episodeNumber)
        TextView episodeNumber;
        @BindView(R.id.textview_episodeName)
        TextView episodeName;
        @BindView(R.id.textview_episodeAirDate)
        TextView episodeAirDate;
        @BindView(R.id.textview_episodeDescription)
        TextView episodeDescription;


        public TvShowDetailsEpisodeViewHolder(View itemView, TvShowEpisodeClickListener listener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick
        public void onEpisodeClick(){
            episodeClickListener.onClick(tvShowEpisodes.get(getAdapterPosition()));
        }
    }

}
