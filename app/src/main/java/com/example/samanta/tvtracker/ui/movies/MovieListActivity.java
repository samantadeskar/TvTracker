package com.example.samanta.tvtracker.ui.movies;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.ui.movies.adapters.MoviesPagerAdapter;
import com.example.samanta.tvtracker.ui.movies.fragments.CommingSoonMoviesFragment;
import com.example.samanta.tvtracker.ui.movies.fragments.NowPlayingMoviesFragment;
import com.example.samanta.tvtracker.ui.movies.fragments.PopularMoviesFragment;
import com.example.samanta.tvtracker.ui.movies.fragments.TopRatedMoviesFragment;
import com.example.samanta.tvtracker.ui.my_profile.MyProfileActivity;
import com.example.samanta.tvtracker.ui.search.SearchMoviesActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.example.samanta.tvtracker.ui.watchlist.WatchlistActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListActivity extends AppCompatActivity {

    private static final String TAG = "TAG_USER";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragmentContanier)
    ViewPager viewPager;

    FirebaseAuth mAuth;
    FirebaseUser user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movielist);
        ButterKnife.bind(this);


        MoviesPagerAdapter adapter = new MoviesPagerAdapter(getSupportFragmentManager());
        List<Fragment> pages = new ArrayList<>();
        pages.add(new PopularMoviesFragment());
        pages.add(new TopRatedMoviesFragment());
        pages.add(new NowPlayingMoviesFragment());
        pages.add(new CommingSoonMoviesFragment());
        adapter.setItems(pages);
        viewPager.setAdapter(adapter);

        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = mAuth.getCurrentUser();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;
            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
                startActivity(new Intent(this, WatchlistActivity.class));
                finish();
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager =
                (SearchManager)getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getInput(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                String query = item.getTitle().toString();

                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...
        return true;
    }

    private void getInput(String query) {
        Intent explicitIntent = new Intent(this, SearchMoviesActivity.class);
        explicitIntent.putExtra("SEARCH_QUERY", query);
        startActivity(explicitIntent);
    }
}