package com.example.samanta.tvtracker.ui.login_register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "login";
    @BindView(R.id.edittext_email_login)
    EditText mEmail;
    @BindView(R.id.edittext_password_login)
    EditText mPassword;
    @BindView(R.id.button_login)
    Button mLogin;
    @BindView(R.id.textview_registernow)
    TextView mRegisterNow;

    private FirebaseAuth mAuth;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public void onStart(){
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            startActivity(new Intent(LoginActivity.this, MovieListActivity.class));
            finish();
        }
    }

    @OnClick(R.id.button_login)
    public void loginUser() {

        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        signIn(email, password);


    }

    private void signIn(String email, String password) {
        final SharedPreferences.Editor editor = getSharedPreferences("user_id", MODE_PRIVATE).edit();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // String currentUser = user.getEmail();
                            // Toast.makeText(LoginActivity.this, currentUser,Toast.LENGTH_SHORT).show();
                            editor.putString("user_id", mAuth.getCurrentUser().getUid().toString());
                            editor.apply();
                            // Toast.makeText(LoginActivity.this,
                            //       getSharedPreferences("user_id", MODE_PRIVATE)
                            //             .getString("user_id","def"), Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(LoginActivity.this, MovieListActivity.class));
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed. Try again",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    @OnClick(R.id.textview_registernow)
    public void goToRegister() {
        startActivity(new Intent(this, RegistrationActivity.class));
        finish();
    }

}
