package com.example.samanta.tvtracker.interaction;

import com.example.samanta.tvtracker.constants.Constants;
import com.example.samanta.tvtracker.networking.ApiService;
import com.example.samanta.tvtracker.pojo.TvShowSeasons;
import com.example.samanta.tvtracker.response.TvShowEpisodeResponse;
import com.example.samanta.tvtracker.response.TvShowResponse;
import com.example.samanta.tvtracker.response.TvShowSeasonsResponse;

import butterknife.OnClick;
import retrofit2.Callback;


public class TvShowInteractorImpl implements TvShowInteractor {

    private final ApiService apiService;

    public TvShowInteractorImpl(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void getPopularTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback) {
        apiService.getPopularTvShow(  page, Constants.API_KEY).enqueue(tvShowResponseCallback);
    }

    @Override
    public void getTopRatedTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback) {
        apiService.getTopRatedTvShow(page, Constants.API_KEY).enqueue(tvShowResponseCallback);
    }

    @Override
    public void getLatestTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback) {
        apiService.getLatestTvShow(page, Constants.API_KEY).enqueue(tvShowResponseCallback);
    }

    @Override
    public void getOnTheAirTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback) {
        apiService.getOnTheAirTvShow(page, Constants.API_KEY).enqueue(tvShowResponseCallback);
    }

    @Override
    public void getSearchedTvShow(int page, Callback<TvShowResponse> tvShowResponseCallback, String query) {
        apiService.getSearchedTvShow(page, Constants.API_KEY, query).enqueue(tvShowResponseCallback);
    }

    @Override
    public void getTvShowSeasons(int tv_id, Callback<TvShowSeasonsResponse> tvShowSeasonsResponseCallback){
        apiService.getTvShowSeasons(tv_id, Constants.API_KEY).enqueue(tvShowSeasonsResponseCallback);
    }

    @Override
    public void getTvShowEpisodes(int tv_id, int season_number, Callback<TvShowEpisodeResponse> tvShowEpisodeResponseCallback){
        apiService.getTvShowEpisodes(tv_id, season_number, Constants.API_KEY).enqueue(tvShowEpisodeResponseCallback);
    }


}
