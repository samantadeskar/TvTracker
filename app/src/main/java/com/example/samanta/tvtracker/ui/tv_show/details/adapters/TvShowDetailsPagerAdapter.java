package com.example.samanta.tvtracker.ui.tv_show.details.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.samanta.tvtracker.ui.tv_show.details.fragments.TvShowDetailsCommentFragment;
import com.example.samanta.tvtracker.ui.tv_show.details.fragments.TvShowDetailsDescriptionFragment;
import com.example.samanta.tvtracker.ui.tv_show.details.fragments.TvShowDetailsSeasonsFragment;

import java.util.ArrayList;
import java.util.List;

public class TvShowDetailsPagerAdapter extends FragmentStatePagerAdapter {

    int numOfTabs;
    private final List<Fragment> fragmentList = new ArrayList<>();

    public TvShowDetailsPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    public void setItems(List<Fragment> fragments) {
        fragmentList.clear();
        fragmentList.addAll(fragments);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TvShowDetailsDescriptionFragment descriptionTab = new TvShowDetailsDescriptionFragment();
                return descriptionTab;
            case 1:
                TvShowDetailsSeasonsFragment seasonsTab = new TvShowDetailsSeasonsFragment();
                return seasonsTab;
            case 2:
                TvShowDetailsCommentFragment commentTab = new TvShowDetailsCommentFragment();
                return commentTab;
            default:
                    TvShowDetailsDescriptionFragment descriptionTab2 = new TvShowDetailsDescriptionFragment();
                    return descriptionTab2;

        }
    }

    @Override
    public int getCount(){
        return fragmentList.size();
    }
}
