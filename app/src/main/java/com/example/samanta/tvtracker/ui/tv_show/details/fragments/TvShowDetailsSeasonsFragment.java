package com.example.samanta.tvtracker.ui.tv_show.details.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.interaction.TvShowInteractor;
import com.example.samanta.tvtracker.listeners.TvShowSeasonClickListener;
import com.example.samanta.tvtracker.networking.RetrofitUtil;
import com.example.samanta.tvtracker.pojo.TvShowSeasons;
import com.example.samanta.tvtracker.response.TvShowSeasonsResponse;
import com.example.samanta.tvtracker.ui.tv_show.details.TvShowEpisodesActivity;
import com.example.samanta.tvtracker.ui.tv_show.details.adapters.TvShowDetailsSeasonsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowDetailsSeasonsFragment extends Fragment {

    private final TvShowInteractor interactor = RetrofitUtil.getTvShowInteractor();
    private TvShowDetailsSeasonsAdapter adapter = new TvShowDetailsSeasonsAdapter();

    @BindView(R.id.recycler_tvShowSeasons)
    RecyclerView recycler_tvShowSeasons;

    private int tv_id;

    TvShowSeasonClickListener tvShowSeasonClickListener = new TvShowSeasonClickListener() {
        @Override
        public void onClick(TvShowSeasons tvShowSeasons) {
            showEpisodes(tvShowSeasons);
        }
    };


    public TvShowDetailsSeasonsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tv_show_details_seasons, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        adapter = new TvShowDetailsSeasonsAdapter(tvShowSeasonClickListener);

        tv_id = getActivity().getIntent().getExtras().getInt("TV_SHOW_ID");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler_tvShowSeasons.setLayoutManager(linearLayoutManager);
        recycler_tvShowSeasons.setItemAnimator(new DefaultItemAnimator());
        recycler_tvShowSeasons.setAdapter(adapter);
        interactor.getTvShowSeasons(tv_id, getTvShowSeasonsCallback());
    }

    @Override
    public void onStart() {
        super.onStart();
        tv_id = getActivity().getIntent().getExtras().getInt("TV_SHOW_ID");
        interactor.getTvShowSeasons(tv_id, getTvShowSeasonsCallback());
    }

    private Callback<TvShowSeasonsResponse> getTvShowSeasonsCallback() {
        return new Callback<TvShowSeasonsResponse>() {
            @Override
            public void onResponse(Call<TvShowSeasonsResponse> call, Response<TvShowSeasonsResponse> response) {
                if (response.isSuccessful()) {
                    adapter.setTvShowSeasons(response.body().getShowSeasons());
                }
            }

            @Override
            public void onFailure(Call<TvShowSeasonsResponse> call, Throwable t) {
                Log.d("TAG", "Fail");
            }
        };
    }

    public void showEpisodes(TvShowSeasons tvShowSeasons){

        Activity activity = this.getActivity();
        Intent intent = new Intent(activity, TvShowEpisodesActivity.class);
        Bundle extras = new Bundle();

        extras.putString("SEASON_NAME", tvShowSeasons.getSeasonName());
        extras.putInt("SEASON_NUMBER", tvShowSeasons.getSeasonNumber());
        extras.putInt("TV_SHOW_ID", tv_id);

        intent.putExtras(extras);
        startActivity(intent);

    }
}
