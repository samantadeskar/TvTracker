package com.example.samanta.tvtracker.ui.watchlist.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.WatchlistClickListener;
import com.example.samanta.tvtracker.pojo.WatchlistMovies;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class WatchlistMoviesAdapter extends RecyclerView.Adapter<WatchlistMoviesAdapter.WatchlistMoviesViewHolder> {


    private WatchlistClickListener watchlistClickListener;
    private final List<WatchlistMovies> watchlists;

    public WatchlistMoviesAdapter() {
        watchlists = new ArrayList<>();

    }

    public WatchlistMoviesAdapter(WatchlistClickListener listener, List<WatchlistMovies> watchlists) {
        this.watchlistClickListener = listener;
        this.watchlists = watchlists;
    }

    public void setWatchlist(List<WatchlistMovies> watchlist) {
        if (watchlist != null) {
            this.watchlists.clear();
            this.watchlists.addAll(watchlist);
            notifyDataSetChanged();
        }
    }

    public void deleteWatchlist(){
        this.watchlists.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WatchlistMoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.watchlist_item, parent, false);
        return new WatchlistMoviesViewHolder(view, watchlistClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchlistMoviesViewHolder holder, int position) {

        WatchlistMovies watchlist = watchlists.get(position);
        if (watchlist.getMoviePoster() != null) {
            Picasso.with(holder.itemView.getContext())
                    .load(watchlist.getMoviePoster())
                    .into(holder.movieTvShowImage);
        }
        holder.movieTvShowTitle.setText(watchlist.getMovieTitle());
        holder.movieTvShowDescription.setText(watchlist.getMovieDescription());
    }


    @Override
    public int getItemCount() {
        return watchlists.size();
    }

    public void addWatchlistMovies(List<WatchlistMovies> watchlists) {
        this.watchlists.addAll(watchlists);
        notifyDataSetChanged();
    }

    class WatchlistMoviesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_movieTvShowImage)
        ImageView movieTvShowImage;
        @BindView(R.id.textview_movieTvShowTitle)
        TextView movieTvShowTitle;
        @BindView(R.id.textview_movieTvShowDescription)
        TextView movieTvShowDescription;


        public WatchlistMoviesViewHolder(View itemView, WatchlistClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        public void onItemClick() {
            watchlistClickListener.onClick(watchlists.get(getAdapterPosition()));
        }

        @OnLongClick
        public boolean onItemLongClick() {
            watchlistClickListener.onLongClick(watchlists.get(getAdapterPosition()));
            return true;
        }


    }
}
