package com.example.samanta.tvtracker.ui.watchlist.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.listeners.WatchlistClickListener;
import com.example.samanta.tvtracker.pojo.WatchlistMovies;
import com.example.samanta.tvtracker.pojo.WatchlistTvShows;
import com.example.samanta.tvtracker.ui.movies.details.MovieDetailsActivity;
import com.example.samanta.tvtracker.ui.watchlist.adapters.WatchlistMoviesAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesFragment extends Fragment {


    private DatabaseReference databaseReference;
    private List<WatchlistMovies> moviesList;
    private FirebaseDatabase database;
    private FirebaseUser user;
    private FirebaseAuth auth;
    WatchlistMovies movies;
    Query query, query2;


    WatchlistMoviesAdapter adapter;

    @BindView(R.id.recycler_watchlisMovies)
    RecyclerView watchlistMovies;


    WatchlistClickListener watchlistClickListener = new WatchlistClickListener() {
        @Override
        public void onClick(WatchlistMovies watchlist) {
            movieDetails(watchlist);
        }

        @Override
        public void onClick(WatchlistTvShows watchlistTvShows) {

        }

        @Override
        public void onLongClick(final WatchlistMovies watchlistMovies) {
            removeFromWatchlist(watchlistMovies);
        }

        @Override
        public void onLongClick(final WatchlistTvShows watchlistTvShows) {

        }
    };

    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movies, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        moviesList = new ArrayList<>();

        adapter = new WatchlistMoviesAdapter(watchlistClickListener, moviesList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        watchlistMovies.setLayoutManager(linearLayoutManager);
        watchlistMovies.setItemAnimator(new DefaultItemAnimator());
        watchlistMovies.setAdapter(adapter);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference().child("watchlistMovies");
        databaseReference.keepSynced(true);
        query = databaseReference.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void fetchData(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null) {
            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                movies = new WatchlistMovies();

                movies.setUserID(ds.getValue(WatchlistMovies.class).getUserID());
                movies.setUserEmail(ds.getValue(WatchlistMovies.class).getUserEmail());
                movies.setMovieTitle(ds.getValue(WatchlistMovies.class).getMovieTitle());
                movies.setMovieReleaseDate(ds.getValue(WatchlistMovies.class).getMovieReleaseDate());
                movies.setMoviePoster(ds.getValue(WatchlistMovies.class).getMoviePoster());
                movies.setMovieDescription(ds.getValue(WatchlistMovies.class).getMovieDescription());
                movies.setMovieID(ds.getValue(WatchlistMovies.class).getMovieID());
                moviesList.add(movies);
            }
            adapter = new WatchlistMoviesAdapter(watchlistClickListener, moviesList);
            watchlistMovies.setAdapter(adapter);
        }
    }

    private void movieDetails(WatchlistMovies watchlist) {
        Activity activity = this.getActivity();
        Intent intent = new Intent(activity, MovieDetailsActivity.class);
        Bundle extras = new Bundle();

        extras.putString("MOVIE_TITLE", watchlist.getMovieTitle());
        extras.putInt("MOVIE_ID", Integer.valueOf(watchlist.getMovieID()));
        extras.putString("MOVIE_POSTER", watchlist.getMoviePoster());
        extras.putString("MOVIE_DESCRIPTION", watchlist.getMovieDescription());
        extras.putString("MOVIE_RELEASE_DATE", watchlist.getMovieReleaseDate());

        intent.putExtras(extras);
        startActivity(intent);

    }

    private void removeFromWatchlist(final WatchlistMovies watchlistMovie) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    query2 = databaseReference.orderByChild("movieID").equalTo(watchlistMovie.getMovieID());
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {

                                builder.setMessage("Do you want to delete "
                                        + watchlistMovie.getMovieTitle() +
                                        " from watchlist?")
                                        .setTitle("Delete from watchlist")
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                                                    ds.getRef().removeValue();
                                                    Toast.makeText(getActivity(),
                                                            watchlistMovie.getMovieTitle() +
                                                                    " sucessfully deleted.",
                                                            Toast.LENGTH_SHORT).show();
                                                    updateView();
                                                }
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(getActivity(), watchlistMovie.getMovieTitle()
                                                                + " was not deleted from watchlist.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void updateView() {

        moviesList.clear();
        adapter = new WatchlistMoviesAdapter(watchlistClickListener, moviesList);
        watchlistMovies.setAdapter(adapter);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    fetchData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}


