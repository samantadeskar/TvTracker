package com.example.samanta.tvtracker.ui.tv_show.details;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.interaction.TvShowInteractor;
import com.example.samanta.tvtracker.networking.RetrofitUtil;
import com.example.samanta.tvtracker.response.TvShowResponse;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.example.samanta.tvtracker.ui.my_profile.MyProfileActivity;
import com.example.samanta.tvtracker.ui.search.SearchTvShowActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.example.samanta.tvtracker.ui.tv_show.details.adapters.TvShowDetailsPagerAdapter;
import com.example.samanta.tvtracker.ui.tv_show.details.fragments.TvShowDetailsCommentFragment;
import com.example.samanta.tvtracker.ui.tv_show.details.fragments.TvShowDetailsDescriptionFragment;
import com.example.samanta.tvtracker.ui.tv_show.details.fragments.TvShowDetailsSeasonsFragment;
import com.example.samanta.tvtracker.ui.watchlist.WatchlistActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_tvShowDetails_tabs)
    TabLayout tabLayout;

    @BindView(R.id.fragmentContanier_tvShowDetails)
    ViewPager viewPager;

    @BindView(R.id.textview_tvShowDetailsTitle)
    TextView tvShowDetailsTitle;

    @BindView(R.id.imageview_tvShowDetailsImage)
    ImageView tvShowDetailImage;

    @BindView(R.id.button_addToWatchlistTvShowDetails)
    Button addToWatchlistTvShowButton;

    @BindView(R.id.button_markAsWatchedTvShow)
    Button markAsWatchedTvShowButton;

    @BindView(R.id.toolbar_TvShowDetails)
    Toolbar toolbar_TvShowDetails;

    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference databaseReference, databaseReference2;
    Query query, query2;

    private final TvShowInteractor interactor = RetrofitUtil.getTvShowInteractor();
    Intent intent;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_show_details);
        ButterKnife.bind(this);

        intent = getIntent();
        extras = intent.getExtras();

        this.tvShowDetailsTitle.setText(extras.getString("TV_SHOW_TITLE"));
        Picasso.with(this.getApplicationContext())
                .load(extras.getString("TV_SHOW_POSTER"))
                .fit()
                .into(tvShowDetailImage);

        tabLayout.addTab(tabLayout.newTab().setText("Description"));
        tabLayout.addTab(tabLayout.newTab().setText("Seasons"));
        tabLayout.addTab(tabLayout.newTab().setText("Comments"));
        TvShowDetailsPagerAdapter adapter = new TvShowDetailsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        List<Fragment> pages = new ArrayList<>();
        pages.add(new TvShowDetailsDescriptionFragment());
        pages.add(new TvShowDetailsSeasonsFragment());
        pages.add(new TvShowDetailsCommentFragment());
        adapter.setItems(pages);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchlistTvShows");
        databaseReference.keepSynced(true);
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("watchedTvShow");
        checkIfWatched();
        checkIfOnWatchlist();
        setSupportActionBar(toolbar_TvShowDetails);

    }

    @Override
    public void onStart(){
        super.onStart();
    }


    @OnClick(R.id.button_addToWatchlistTvShowDetails)
    public void addToWatchlistAlert(){

        checkIfOnWatchlist();
        final String tvShowTitle = extras.getString("TV_SHOW_TITLE");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to add " + tvShowTitle + " to watchlist?")
                .setTitle("Add to watchlist")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addToWatchlist();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(TvShowDetailsActivity.this, tvShowTitle + " was not added to watchlist.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
        checkIfOnWatchlist();
    }

    private void addToWatchlist(){

        DatabaseReference newEntry = databaseReference.push();

        String tvShowID = String.valueOf(extras.getInt("TV_SHOW_ID"));
        String userID = user.getUid();
        String userEmail = user.getEmail();
        final String tvShowTitle = extras.getString("TV_SHOW_TITLE");
        String tvShowPoster = extras.getString("TV_SHOW_POSTER");
        String tvShowDescription = extras.getString("TV_SHOW_DESCRIPTION");
        String tvShowFirstAirDate = extras.getString("TV_SHOW_RELEASE_DATE");
        String tvShowNumberOfSeasons = extras.getString("TV_SHOW_NUMBER_OF_SEASONS");
        String tvShowNumberOfEpisodes = extras.getString("TV_SHOW_NUMBER_OF_EPISODES");

        Map<String,String> dataToSave = new HashMap<>();
        dataToSave.put("tvShowID", tvShowID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);
        dataToSave.put("tvShowTitle", tvShowTitle);
        dataToSave.put("tvShowPoster", tvShowPoster);
        dataToSave.put("tvShowDescription", tvShowDescription);
        dataToSave.put("tvShowFirstAirDate", tvShowFirstAirDate);
        dataToSave.put("numberOfSeasons", tvShowNumberOfSeasons);
        dataToSave.put("numberOfEpisodes", tvShowNumberOfEpisodes);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(TvShowDetailsActivity.this,
                        tvShowTitle + " successfully added to watchlist.",
                        Toast.LENGTH_SHORT).show();
                checkIfOnWatchlist();
            }
        });
    }

    @OnClick(R.id.button_markAsWatchedTvShow)
    public void markAsWatchedTvShow(){
        checkIfWatched();
        DatabaseReference newEntry = databaseReference2.push();

        String tvShowID = String.valueOf(extras.getInt("TV_SHOW_ID"));
        String userID = user.getUid();
        String userEmail = user.getEmail();
        final String tvShowTitle = extras.getString("TV_SHOW_TITLE");
        String tvShowPoster = extras.getString("TV_SHOW_POSTER");
        String tvShowDescription = extras.getString("TV_SHOW_DESCRIPTION");
        String tvShowFirstAirDate = extras.getString("TV_SHOW_RELEASE_DATE");
        String tvShowNumberOfSeasons = extras.getString("TV_SHOW_NUMBER_OF_SEASONS");
        String tvShowNumberOfEpisodes = extras.getString("TV_SHOW_NUMBER_OF_EPISODES");

        Map<String,String> dataToSave = new HashMap<>();
        dataToSave.put("tvShowID", tvShowID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);
        dataToSave.put("tvShowTitle", tvShowTitle);
        dataToSave.put("tvShowPoster", tvShowPoster);
        dataToSave.put("tvShowDescription", tvShowDescription);
        dataToSave.put("tvShowFirstAirDate", tvShowFirstAirDate);
        dataToSave.put("numberOfSeasons", tvShowNumberOfSeasons);
        dataToSave.put("numberOfEpisodes", tvShowNumberOfEpisodes);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(TvShowDetailsActivity.this, tvShowTitle + " marked as watched",
                        Toast.LENGTH_SHORT).show();
            }
        });
        checkIfWatched();
    }

    private void checkIfWatched(){

        query = databaseReference2.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    query2 = databaseReference2.orderByChild("tvShowID").equalTo(String.valueOf(extras.getInt("TV_SHOW_ID")));
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                markAsWatchedTvShowButton.setText("Watched");
                                markAsWatchedTvShowButton.setEnabled(false);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void checkIfOnWatchlist(){

        query = databaseReference.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    query2 = databaseReference.orderByChild("tvShowID").equalTo(String.valueOf(extras.getInt("TV_SHOW_ID")));
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                addToWatchlistTvShowButton.setText("On watchlist");
                                addToWatchlistTvShowButton.setEnabled(false);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;

            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
                startActivity(new Intent(this, WatchlistActivity.class));
                finish();
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager =
                (SearchManager)getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getInput(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...

        return true;
    }

    private void getInput(String query) {
        Intent explicitIntent = new Intent(this, SearchTvShowActivity.class);
        explicitIntent.putExtra("SEARCH_TVSHOW_QUERY", query);
        startActivity(explicitIntent);
    }
}
