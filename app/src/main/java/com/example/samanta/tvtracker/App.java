package com.example.samanta.tvtracker;

import android.app.Application;
import android.support.annotation.NonNull;

import com.example.samanta.tvtracker.constants.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    private static Retrofit retrofit;
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }


}
