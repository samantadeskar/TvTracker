package com.example.samanta.tvtracker.ui.movies.details;


import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samanta.tvtracker.R;
import com.example.samanta.tvtracker.ui.movies.MovieListActivity;
import com.example.samanta.tvtracker.ui.movies.details.adapters.MovieDetailsPagerAdapter;
import com.example.samanta.tvtracker.ui.movies.details.fragments.MovieDetailsCommentFragment;
import com.example.samanta.tvtracker.ui.movies.details.fragments.MovieDetailsDescriptionFragment;
import com.example.samanta.tvtracker.ui.my_profile.MyProfileActivity;
import com.example.samanta.tvtracker.ui.search.SearchMoviesActivity;
import com.example.samanta.tvtracker.ui.tv_show.TvShowActivity;
import com.example.samanta.tvtracker.ui.watchlist.WatchlistActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MovieDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_movieDetails_tabs)
    TabLayout tabLayout;
    @BindView(R.id.fragmentContanier_movieDetails)
    ViewPager viewPager;
    @BindView(R.id.textview_movieDetailsTitle)
    TextView movieDetailsTitle;
    @BindView(R.id.imageview_movieDetailsImage)
    ImageView movieImage;
    @BindView(R.id.button_markAsWatched)
    Button markAsWatched;
    @BindView(R.id.button_addToWatchlistMovieDetails)
    Button addToWatchlistButton;
    @BindView(R.id.toolbar_movieDetails)
    Toolbar toolbar_movieDetails;

    private FirebaseAuth auth;
    private DatabaseReference databaseReference, databaseReference2;
    private FirebaseUser user;
    Query query, query2;
    Intent intent;
    Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);

        intent = getIntent();
        extras = intent.getExtras();

        this.movieDetailsTitle.setText(extras.getString("MOVIE_TITLE"));
        Picasso.with(this.getApplicationContext())
                .load(extras.getString("MOVIE_POSTER"))
                .fit()
                .into(movieImage);


        tabLayout.addTab(tabLayout.newTab().setText("Description"));
        tabLayout.addTab(tabLayout.newTab().setText("Comments"));
        MovieDetailsPagerAdapter adapter = new MovieDetailsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        List<Fragment> pages = new ArrayList<>();
        pages.add(new MovieDetailsDescriptionFragment());
        pages.add(new MovieDetailsCommentFragment());
        adapter.setItems(pages);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("watchlistMovies");
        databaseReference.keepSynced(true);
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("watchedMovies");

        checkIfWatched();
        checkIfOnWatchlist();

        setSupportActionBar(toolbar_movieDetails);

    }


    @OnClick(R.id.button_addToWatchlistMovieDetails)
    public void addToWatchlistAlert() {

        checkIfOnWatchlist();

        final String movieTitle = extras.getString("MOVIE_TITLE");
        AlertDialog.Builder builder = new AlertDialog.Builder(MovieDetailsActivity.this);
        builder.setMessage("Do you want to add " + movieTitle + " to watchlist?")
                .setTitle("Add to watchlist")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addToWatchlist();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(MovieDetailsActivity.this, movieTitle + " was not added to watchlist.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .show();

        checkIfOnWatchlist();
    }


    private void addToWatchlist() {

        DatabaseReference newEntry = databaseReference.push();

        String movieID = String.valueOf(extras.getInt("MOVIE_ID"));
        String userID = user.getUid();
        String userEmail = user.getEmail();
        final String movieTitle = extras.getString("MOVIE_TITLE");
        String moviePoster = extras.getString("MOVIE_POSTER");
        String movieDescription = extras.getString("MOVIE_DESCRIPTION");
        String movieReleaseDate = extras.getString("MOVIE_RELEASE_DATE");

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("movieID", movieID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);
        dataToSave.put("movieTitle", movieTitle);
        dataToSave.put("moviePoster", moviePoster);
        dataToSave.put("movieDescription", movieDescription);
        dataToSave.put("movieReleaseDate", movieReleaseDate);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(MovieDetailsActivity.this, movieTitle + " succesfully added to watchlist",
                        Toast.LENGTH_SHORT).show();
                checkIfOnWatchlist();
            }
        });
    }

    @OnClick(R.id.button_markAsWatched)
    public void markAsWatchedMovie() {

        checkIfWatched();

        DatabaseReference newEntry = databaseReference2.push();

        String movieID = String.valueOf(extras.getInt("MOVIE_ID"));
        String userID = user.getUid();
        String userEmail = user.getEmail();
        final String movieTitle = extras.getString("MOVIE_TITLE");
        String moviePoster = extras.getString("MOVIE_POSTER");
        String movieDescription = extras.getString("MOVIE_DESCRIPTION");
        String movieReleaseDate = extras.getString("MOVIE_RELEASE_DATE");

        Map<String, String> dataToSave = new HashMap<>();
        dataToSave.put("movieID", movieID);
        dataToSave.put("userID", userID);
        dataToSave.put("userEmail", userEmail);
        dataToSave.put("movieTitle", movieTitle);
        dataToSave.put("moviePoster", moviePoster);
        dataToSave.put("movieDescription", movieDescription);
        dataToSave.put("movieReleaseDate", movieReleaseDate);

        newEntry.setValue(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(MovieDetailsActivity.this, movieTitle + " marked as watched",
                        Toast.LENGTH_SHORT).show();
            }
        });
        checkIfWatched();
    }

    private void checkIfWatched() {

        query = databaseReference2.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    query2 = databaseReference2.orderByChild("movieID").equalTo(String.valueOf(extras.getInt("MOVIE_ID")));
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                markAsWatched.setText("Watched");
                                markAsWatched.setEnabled(false);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }


                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void checkIfOnWatchlist() {

        query = databaseReference.orderByChild("userID").equalTo(user.getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    query2 = databaseReference.orderByChild("movieID").equalTo(String.valueOf(extras.getInt("MOVIE_ID")));
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot.exists()) {
                                addToWatchlistButton.setText("On watchlist");
                                addToWatchlistButton.setEnabled(false);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }


                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.showMovies:
                startActivity(new Intent(this, MovieListActivity.class));
                finish();
                return true;
            case R.id.showTvShows:
                startActivity(new Intent(this, TvShowActivity.class));
                finish();
                return true;
            case R.id.watchlist:
                startActivity(new Intent(this, WatchlistActivity.class));
                finish();
                return true;
            case R.id.myProfile:
                startActivity(new Intent(this, MyProfileActivity.class));
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager =
                (SearchManager)getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getInput(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


        // Configure the search info and add any event listeners...
        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                String query = item.getTitle().toString();

                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_search);


        // Any other things you have to do when creating the options menu...
        return true;
    }
    private void getInput(String query) {
        Intent explicitIntent = new Intent(this, SearchMoviesActivity.class);
        explicitIntent.putExtra("SEARCH_QUERY", query);
        startActivity(explicitIntent);
    }

}